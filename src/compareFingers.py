#!/usr/bin/env python

from cpp_interface import Cpp_interface
import argparse
from PIL import Image

parser = argparse.ArgumentParser()
parser.add_argument("--f1", type=str, help="finger1")
parser.add_argument("--f2", type=str, help="finger2")
args = parser.parse_args()


cpp = Cpp_interface()

print(cpp.compare_pils(Image.open(args.f1), Image.open(args.f2)))
