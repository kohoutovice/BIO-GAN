from torch.utils.data import Dataset
import requests
from os import path
from zipfile import ZipFile
from PIL import Image
import glob


class Dataset1(Dataset):
    def __init__(self, name="Real", transform=None):
        url = "https://gitlab.com/kohoutovice/datasets/-/raw/master/" + name + ".zip"
        self.transform = transform
        self.filename = url.split("/")[-1]
        if not path.exists(self.filename):
            print("Downloading...", url)
            downloaded_obj = requests.get(url)
            with open(self.filename, "wb") as file:
                file.write(downloaded_obj.content)

        if not path.exists(self.filename.split(".")[0]):
            with ZipFile(self.filename, 'r') as zipObj:
                print("Extracting...")
                # Extract all the contents of zip file in current directory
                zipObj.extractall()

        self.data = []
        for image in glob.glob(self.filename.split(".")[0] + "/*.BMP"):
            temp = Image.open(image)
            temp = temp.crop((2, 2, temp.size[0]-4, temp.size[1]-4))
            temp = temp.convert('L')
            self.data.append(temp.copy())
            temp.close()

    def __getitem__(self, index):
        item = self.transform(self.data[index]) if self.transform else self.data[index]
        return item

    def get_image(self, index):
        return self.data[index]


    def size_of_dataset(self):
        return len(self.data)

    def __len__(self):
        return len(self.data)
