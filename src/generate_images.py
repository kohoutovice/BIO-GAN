from network import Network
import torch
from PIL import Image
import argparse
from utils import savePilImages


device = 'cuda' if torch.cuda.is_available() else 'cpu'


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--count", type=int, default=32, help="number of images to generate")
    parser.add_argument("--folder", default="images/", help="where to generate")
    parser.add_argument("--network", default=None, choices=Network.available(), help="select network to inference on")
    args = parser.parse_args()

    net = Network()
    if args.network:
        net = Network(args.network)
    nz_size = net.nz_size()
    images = net(torch.randn(args.count, nz_size, device=device))
    savePilImages(images, args.folder)


def main1():

    parser = argparse.ArgumentParser()
    parser.add_argument("--count", type=int, default=32, help="number of images to generate")
    parser.add_argument("--folder", default="images/", help="where to generate")
    parser.add_argument("--divider", type=int, default=100, help="divide randn by")
    parser.add_argument("--ichange", type=int, default=100, help="divide randn by")
    parser.add_argument("--network", default=None, choices=Network.available(), help="select network to inference on")
    args = parser.parse_args()
    
    net = Network()
    if args.network:
        net = Network(args.network)

    nz_size = net.nz_size()
    final_images = []

    latent = torch.randn(1, nz_size, device=device)
    last_latent = latent[:]

    increment = torch.randn(1, nz_size, device=device)/args.divider

    for i in range(args.count):
        print(i)
        final_images += net(latent)
        last_latent = latent.clone().detach()
        latent += increment

        if i % args.ichange == 0 and i >0:
            increment = torch.randn(1, nz_size, device=device)/args.divider

    savePilImages(final_images, args.folder)


if __name__ == '__main__':
    main()
