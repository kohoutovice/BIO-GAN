from network import Network
import torch
from matplotlib.pyplot import imshow
from PIL import Image
from cpp_interface import Cpp_interface
import argparse
import pdb
import os

device = 'cuda' if torch.cuda.is_available() else 'cpu'

class Genetic_algorithm:

    def __init__(self, net, args):
        self.net = net
        self.batch_size = args.batch_size
        self.nz_size = net.nz_size()
        self.generations = args.generations
        self.comparator = Cpp_interface()
        self.strategy = args.strategy
        self.offsprings = args.offsprings
        self.mutation_probability = args.mutation_probability
        self.mutation_force = args.mutation_force

    def __del__(self):
        last_image = self.get_best_image()
        last_image.save( str(self.best_score) + "last.BMP")

    def get_best_image(self):
        index = self.population_scores.index(max(self.population_scores))
        last_image = self.population[index]
        return last_image

    def first_generation(self, goal):
        self.goal = goal
        self.best_score = 0
        self.parent = None
        self.population_scores = []

        self.population_tensors, self.population = self.generate_population()
        self.evaluate_population()

    def generate_population(self):
        random_tensors = torch.randn(self.batch_size, self.nz_size, device=device)
        return random_tensors, self.net(random_tensors)

    def evaluate_population(self):
        scores = self.comparator.compate_batch_with_single(self.population, self.goal)
        self.best_score = max(scores + [self.best_score])
        self.population_scores = scores

    def mutate_population(self, soft_mutation):
        mutated_pop = []
        parent = self.parent
        for _ in range(self.batch_size):
            new = parent.clone()
            mutation_probs = torch.rand(self.nz_size)
            for index, mutation_prob in enumerate(mutation_probs):
                if mutation_prob <= self.mutation_probability:
                    if soft_mutation:
                        new[index] = new[index] + (torch.randn(1) * self.mutation_force)
                    else:
                        new[index] = torch.randn(1)

            mutated_pop.append(new)

        return torch.stack(mutated_pop)

    def block_mutation(self, soft_mutation):
        mutated_pop = []
        for _ in range(self.batch_size):
            new = self.parent.clone()
            indexes = torch.randint(self.nz_size,(2,))
            mutation_start = min (indexes).item()
            mutation_end = max(indexes).item()
            if soft_mutation == False:
                self.mutation_force = 1
            new_block = torch.randn(mutation_end - mutation_start,device=device) * self.mutation_force
            mutated_pop.append(torch.cat((new[0:mutation_start], new_block, new[mutation_end:] ), 0))

        return torch.stack(mutated_pop)


    def soft_tensor_mutation(self):
        # This method ignores parent, beacause it selects population_size / offsprings best
        parents_indexes = sorted( [(x,i) for (i,x) in enumerate(self.population_scores)], reverse=True)[:self.batch_size // self.offsprings]
        parents = [self.population_tensors[index] for ( _, index) in parents_indexes]

        mutated_pop = []
        mutation_tensors = torch.randn(self.batch_size, self.nz_size, device=device) * self.mutation_force
        for i in range(self.batch_size):
            if i % self.offsprings == 0:
                parent = parents[i//self.offsprings]
                mutated_pop.append(parent)
            else:
                mutated_pop.append(parent + mutation_tensors[i])
        return torch.stack(mutated_pop)

    def create_population_tensors(self):
        strategy = self.strategy

        if strategy == "random_mutation":
            return self.mutate_population(False)
        elif strategy ==  "random_search":
            return torch.randn(self.batch_size, self.nz_size, device=device)
        elif strategy == "soft_random_mutation":
            return self.mutate_population(True)
        elif strategy ==  "block_mutation":
            return self.block_mutation(False)
        elif strategy ==  "soft_block_mutation":
            return self.block_mutation(True)
        elif strategy == "soft_tensor_mutation":
            return self.soft_tensor_mutation()
        else:
            raise NotImplementedError()


    def regenerate_and_eval_population(self, generation):
        print(f"Generation {generation} best: {self.best_score}", end = "")
        #Take the best.
        if max(self.population_scores) >= self.best_score:
            best_index = self.population_scores.index(self.best_score)
            self.parent = self.population_tensors[best_index]
            print("")
        else:
            print(f" <- generation without progress :(")

        self.population_tensors = self.create_population_tensors()
        self.population = self.net(self.population_tensors)
        self.evaluate_population()


    def run_generations(self, image):
        self.first_generation(image)

        for gen in range(self.generations):
            self.regenerate_and_eval_population(gen)

        return self.best_score, self.parent


def print_results(results, args):
    print(f"With popultion size {args.batch_size} with {args.generations} generations, mutation probability {args.mutation_probability}",
    f"mutation force {args.mutation_force}, net {args.network} and evaluation strategy {args.strategy} achieved results:")
    for run_spec, run_result in results.items():
        print(run_spec, " : ", run_result, "+\\")

def create_folder_for_results(args):
    try:
        os.makedirs(args.outf)
    except OSError:
        pass

def arguments_ok(args):
    if(args.strategy == "soft_tensor_mutation" and args.batch_size % args.offsprings != 0):
        print("Unable to calculat number of parrents.")
        return False
    if args.mutation_force > 1:
        print("Mutation force above 1? That would be dangerous.")
        return False

def main():
    ga_types = ["random_search", "random_mutation", "soft_random_mutation", "block_mutation", "soft_block_mutation", "soft_tensor_mutation"]

    parser = argparse.ArgumentParser()
    parser.add_argument("-b", "--batch_size", type=int, default=128, help="batch size and population size")
    parser.add_argument("-m", "--mutation_probability", type=float, default=0.01, help="probability of mutating each element in input tensor")
    parser.add_argument("-s", "--strategy", default="random_increment",choices=ga_types, help="ga evaluation strategy")
    parser.add_argument("-a", "--array_of_images", default="", type=lambda s: [item for item in s.split(',')],
            help="path to multiple images separated by comma")
    parser.add_argument("-o", "--offsprings", type=int, default=8, help="number of childern for strategy soft_tensor_mutation")
    parser.add_argument("-r", "--runs", type=int, default=1, help="number of runs for single image")
    parser.add_argument("-g", "--generations", type=int, default=100, help="number of generations to be run")
    parser.add_argument("-f", "--mutation_force", type=float, default=0.25, help="when using soft mutations, how soft shoud mutation be")
    parser.add_argument("--outf", type=str, default="inference_images/", help="images store folder")
    parser.add_argument("-n", "--network", default=None, choices=Network.available(), help="select network to inference on")
    args = parser.parse_args()

    if arguments_ok(args) == False:
        return

    create_folder_for_results(args)

    net = Network()
    if args.network:
        net = Network(args.network)

    # Ga init.
    ga = Genetic_algorithm(net, args)

    results_summary = {}
    for run in range(args.runs):
        for im in args.array_of_images:
            goal = Image.open(im).convert('L')
            best_score, parent = ga.run_generations(goal)
            best_image = ga.get_best_image()
            best_image.save(args.outf.rstrip("/") + "/" + im.split("/")[-1] + "_run_" + str(run) + ".bmp")
            results_summary[im + " run " + str(run)] = best_score

    print_results(results_summary, args)


if __name__ == '__main__':
    main()
