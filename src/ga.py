from matplotlib.pyplot import imshow
import random
import os
from dataset import Dataset1
import argparse
from PIL import Image
from cpp_interface import Cpp_interface
from numpy import asarray
import numpy as np
import pdb

class Genetic_algorithm:

    def __init__(self, goal_im, init_images, square_size, comparator):
        self.comparator = comparator
        self.square_size = square_size
        self.temp_name = "ga_tmp.tiff"
        self.goal_name = "goal.tiff"
        self.pop_size = 100
        self.goal = goal_im
        self.goal.resize((504,480)).save(self.goal_name,format="TIFF", compression="tif_lzw", dpi=(504, 480))

        self.population = self.generate_population(init_images)
        self.population_scores = self.evaluate_population()

    def __del__(self):
        if os.path.exists(self.temp_name):
            os.remove(self.temp_name)

    def save_best(self,name):
        self.population_scores = self.evaluate_population()
        index = self.population_scores.index(max(self.population_scores))
        last_image = Image.fromarray(self.population[index])
        last_image.resize((504,480)).save(name,format="TIFF", compression="tif_lzw", dpi=(504, 480))

    def im_eval(self, im_arr):
        im = Image.fromarray(im_arr)
        im.resize((504,480)).save(self.temp_name,format="TIFF", compression="tif_lzw", dpi=(504, 480))
        score = self.comparator.compare_tifs(self.temp_name, self.goal_name)
        return score

    def generate_population(self, init_images):
        init_pop = []
        for im in init_images:
            im = im.resize((100,100))
            init_pop.append(asarray(im.copy()))

        return init_pop

    def evaluate_population(self):
        return [self.im_eval(im) for im in self.population]


    def select_best(self, how_many):
        parents_indexes = sorted( [(x,i) for (i,x) in enumerate(self.population_scores)], reverse=True)[:how_many]
        parents = [self.population[index] for ( _, index) in parents_indexes]
        return parents

    def get_new_unmutated_population(self):
        population = self.select_best(10)
        for i in range(10):
            for j in range(9):
                population.append(np.copy(population[i]))
        return population

    def mutate_population_whole(self):
        population = np.array(self.get_new_unmutated_population())
        zeros= np.zeros((10,100,100), dtype="uint8")
        noise = np.random.randint(0,5,(90,100,100), dtype="uint8")
        new_population = np.add(population, np.concatenate((zeros,noise)))

        return new_population

    def mutate_square(self,unit):
            square_size = self.square_size
            noise = np.random.randint(0,256, (square_size, square_size),dtype="uint8")
            x,y = np.random.randint(0,101 - square_size, (2))
            unit[x:x+square_size,y:y+square_size] = noise
            return unit

    def mutate_population_square(self):
        population = self.get_new_unmutated_population()
        mutated = population[:10] + [self.mutate_square(np.copy(p)) for p in population[10:]]
        return mutated

    def run(self, generations):
        maximum = 0
        for gen in range(generations):
            self.population =  self.mutate_population_square()
            self.population_scores = self.evaluate_population()
            if max(self.population_scores) > maximum:
                maximum = max(self.population_scores)
            if gen % 100 == 0:
                print("Gen ", gen, " score ", maximum)
            if maximum > 200:
                print("Achieved max in generation ", gen)
                self.save_best("yohoo.tiff")
                return maximum

        return maximum


def one_image_run(im,index, init_images, args, comparator):
        ga = Genetic_algorithm(im,init_images, args.square_size, comparator)
        score = ga.run(args.generations_count)
        ga.save_best("guessed_" + str(index) + ".tiff")
        im.resize((504,480)).save("input_" + str(index) + ".tiff",format="TIFF", compression="tif_lzw", dpi=(504, 480))
        return score

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--square_size", type=int, default=3, help="mutation square size")
    parser.add_argument("-c", "--images_count", type=int, default=5, help="How many images to goues - by default its 100 - 100+x image from dataset")
    parser.add_argument("-g", "--generations_count", type=int, default=5000, help="how many generations to spend on an image")
    args = parser.parse_args()

    dataset = Dataset1()
    print("Dataset initializon finished.")
    comparator = Cpp_interface()

    total_images = dataset.size_of_dataset()
    random_image_indexes = random.sample(range(0, total_images), 100 + args.images_count)

    images = [dataset.get_image(i) for i in random_image_indexes[0:args.images_count]]
    init_images = [dataset.get_image(i) for i in random_image_indexes[args.images_count:]]


    images_scores = []
    for index, im in enumerate(images):
        images_scores.append(one_image_run(im,index, init_images, args, comparator))

    print(images_scores)


if __name__ == '__main__':
    main()

