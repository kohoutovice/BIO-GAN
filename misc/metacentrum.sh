#!/usr/bin/env bash


#ssh xjurca08@skirit.metacentrum.cz
#qsub -I -q gpu -l select=1:ncpus=2:ngpus=1:mem=16gb:scratch_local=30GB:gpu_cap=cuda61 -l walltime=24:00:00


module add cudnn-7.6.4-cuda10.1
module add python-3.6.2-gcc
cd $SCRATCHDIR || exit 1
mkdir temp
JOB_DIR=$(pwd)
TMPDIR=$(pwd)/temp
export TMPDIR

pip3.6 install torch==1.6.0 torchvision==0.8.1 --cache-dir $(pwd)/cache --root $(pwd) --user
pip3.6 install python-Levenshtein --cache-dir $(pwd)/cache --root $(pwd) --user
pip3.6 install lmdb --cache-dir $(pwd)/cache --root $(pwd) --user
pip3.6 install opencv-python --cache-dir $(pwd)/cache --root $(pwd) --user
PYTHONPATH=$(pwd)/storage/praha1/home/$USER/.local/lib/python3.6/site-packages/:$PYTHONPATH
export PYTHONPATH


git clone https://gitlab.com/kohoutovice/BIO-GAN.git

cd BIO-GAN/utils

source env.sh

bash create_cpp_interface.sh

cd ..

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$JOB_DIR/BIO-GAN/utils/FingerCell_3_3_SDK/Lib/Linux_x86_64
