#include <TutorialUtils.hpp>

#include <NCore.hpp>
#include <FingerCell.hpp>
#include <NMedia.hpp>
#include <NLicensing.hpp>

using namespace std;
using namespace Neurotec;
using namespace Neurotec::Licensing;
using namespace Neurotec::IO;
using namespace Neurotec::Media;
using namespace Neurotec::Images;

const NChar title[] = N_T("FCEnrollFingerFromImageCPP");
const NChar description[] = N_T("Demonstrates fingerprint feature extraction from image.");
const NChar version[] = N_T("3.3.0.0");
const NChar copyright[] = N_T("Copyright (C) 2018-2020 Neurotechnology");

int usage()
{
	cout << "usage:" << endl;
	cout << "\t" << title << " [image] [template]" << endl << endl;
	cout << "\t[image]    - image filename to extract." << endl;
	cout << "\t[template] - filename to store extracted features." << endl;
	return 1;
}

int main(int argc, NChar **argv)
{
	const NChar * license = { N_T("FingerCell") };
	OnStart(title, description, version, copyright, argc, argv);

	if (argc < 3)
	{
		OnExit();
		return usage();
	}

	//=========================================================================
	// TRIAL MODE
	//=========================================================================
	// Below code line determines whether TRIAL is enabled or not. To use purchased licenses, don't use below code line.
	// GetTrialModeFlag() method takes value from "Bin/Licenses/TrialFlag.txt" file. So to easily change mode for all our examples, modify that file.
	// Also you can just set TRUE to "TrialMode" property in code.

	NLicenseManager::SetTrialMode(GetTrialModeFlag());
	cout << "Trial mode: " << NLicenseManager::GetTrialMode() << endl;
	//=========================================================================

	try
	{
		if (!NLicense::Obtain(N_T("/local"), N_T("5000"), license))
		{
			NThrowException(NString::Format(N_T("Could not obtain licenses: {S}"), license));
		}
		
		::Neurotec::FingerCell::FingerCell fingerCell;
		NImage image = NImage::FromFile(argv[1]);
		NBuffer record = fingerCell.Extract(image);
		NFile::WriteAllBytes(argv[2], record);
		cout << "Template successfully extracted and saved to file." << endl;
	}
	catch (NError& ex)
	{
		return LastError(ex);
	}

	OnExit();
	return 0;
}
