set -xe

python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.1 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f1.txt
python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.3 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f3.txt
python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.5 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f5.txt
python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.7 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f7.txt
python3 inference.py -s soft_tensor_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.9 >/storage/brno2/home/xkadle35/result_soft_tensor_mutation_f9.txt

python3 inference.py -s soft_block_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.1 >/storage/brno2/home/xkadle35/result_soft_block_mutation_f1.txt
python3 inference.py -s soft_block_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.3 >/storage/brno2/home/xkadle35/result_soft_block_mutation_f3.txt
python3 inference.py -s soft_block_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.5 >/storage/brno2/home/xkadle35/result_soft_block_mutation_f5.txt
python3 inference.py -s soft_block_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.7 >/storage/brno2/home/xkadle35/result_soft_block_mutation_f7.txt
python3 inference.py -s soft_block_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.9 >/storage/brno2/home/xkadle35/result_soft_block_mutation_f9.txt

python3 inference.py -s soft_random_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.1 >/storage/brno2/home/xkadle35/result_soft_random_mutation_f1.txt
python3 inference.py -s soft_random_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.3 >/storage/brno2/home/xkadle35/result_soft_random_mutation_f3.txt
python3 inference.py -s soft_random_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.5 >/storage/brno2/home/xkadle35/result_soft_random_mutation_f5.txt
python3 inference.py -s soft_random_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.7 >/storage/brno2/home/xkadle35/result_soft_random_mutation_f7.txt
python3 inference.py -s soft_random_mutation -a example/2.BMP -o 8 -b 128 -g 100 -r 10 -f 0.9 >/storage/brno2/home/xkadle35/result_soft_random_mutation_f9.txt
